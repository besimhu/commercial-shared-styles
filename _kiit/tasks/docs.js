/*
 * @title docs
 * @description Task to compile markup via Twig.js
 */

// Dependencies
import { src, dest } from 'gulp';
import fs from 'fs';
import twig from 'gulp-twig';
import data from 'gulp-data';
import twigMarkdown from 'twig-markdown';
import browserSync from 'browser-sync';
import handleErrors from '../utils/handleErrors';
import projectPath from '../utils/projectPath';

// Config
import { config, paths } from '../config';

// Data Object
const getData = () => {
  const jsonData = {
    global: JSON.parse(fs.readFileSync(projectPath(paths.src, 'docs/_data/global.json')), 'utf8'),
    options: JSON.parse(fs.readFileSync(projectPath(paths.src, 'docs/_data/options.json')), 'utf8'),
  };

  return jsonData;
};

// Task
const docs = () => {
  return src([config.docs.src, config.docs.excludeFolders])
    .pipe(data(getData))
    .on('error', handleErrors)
    .pipe(
      twig({
        base: `${paths.src}/docs/`,
        data: data(getData),
        extend: twigMarkdown
      })
    )
    .pipe(dest(config.docs.dest))
    .on('error', handleErrors)
    .pipe(browserSync.stream());
};

export default docs;
